﻿CREATE TABLE [bds].[CLIENTES] (
  [UNIDADECONOMICA] [varchar](10) NULL,
  [FLGORIGEN] [varchar](10) NULL,
  [FLGCHECK] [varchar](10) NULL,
  [CAMPANA] [varchar](50) NULL,
  [ID] [varchar](50) NULL,
  [FECREGISTRO] [varchar](8) NULL,
  [DUI] [nvarchar](50) NULL,
  [ID_TIPODUI] [varchar](2) NOT NULL,
  [DUIval] [nvarchar](1) NULL,
  [NOMBRES] [varchar](200) NULL,
  [APELLIDOS] [varchar](200) NULL,
  [NOMBRES_APELLIDOS] [varchar](400) NULL,
  [TELEFONO] [varchar](50) NULL,
  [TIPOTELEFONO] [nvarchar](50) NULL,
  [EMAIL] [nvarchar](100) NULL,
  [DIRECCION] [varchar](400) NULL,
  [DISTRITO] [varchar](150) NULL,
  [FECNACIMIENTO] [varchar](8) NULL,
  [EDAD] [nvarchar](max) NULL,
  [SEXO] [varchar](9) NULL,
  [INFORMACION1] [varchar](200) NULL,
  [INFORMACION2] [varchar](200) NULL,
  [FLG_TERM_COND] [varchar](50) NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO