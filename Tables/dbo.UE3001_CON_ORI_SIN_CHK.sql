﻿CREATE TABLE [dbo].[UE3001_CON_ORI_SIN_CHK] (
  [Campaña] [varchar](50) NULL,
  [ID] [varchar](50) NULL,
  [Nombre] [varchar](50) NULL,
  [Apellidos] [varchar](50) NULL,
  [Nombres y Apellidos] [varchar](100) NULL,
  [DNI] [varchar](50) NULL,
  [Edad] [varchar](50) NULL,
  [Sexo] [varchar](50) NULL,
  [Teléfono] [varchar](50) NULL,
  [Dirección] [varchar](150) NULL,
  [Distrito] [varchar](150) NULL,
  [Fecha Nacimiento] [varchar](50) NULL,
  [Correo] [varchar](100) NULL,
  [Fecha Registro] [varchar](50) NULL,
  [Información de qué tipo de categoría le gustaría recibir (se] [varchar](50) NULL,
  [Qué tipo de información le gustaría recibir] [varchar](100) NULL
)
ON [PRIMARY]
GO