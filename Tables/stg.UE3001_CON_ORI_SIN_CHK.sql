﻿CREATE TABLE [stg].[UE3001_CON_ORI_SIN_CHK] (
  [CAMPANA] [varchar](50) NULL,
  [ID] [varchar](50) NULL,
  [NOMBRES] [varchar](200) NULL,
  [APELLIDOS] [varchar](200) NULL,
  [NOMBRES_APELLIDOS] [varchar](400) NULL,
  [DNI] [varchar](50) NULL,
  [EDAD] [varchar](50) NULL,
  [SEXO] [varchar](50) NULL,
  [TELEFONO] [varchar](50) NULL,
  [DIRECCION] [varchar](400) NULL,
  [DISTRITO] [varchar](150) NULL,
  [FECNACIMIENTO] [varchar](50) NULL,
  [EMAIL] [varchar](100) NULL,
  [FECREGISTRO] [varchar](50) NULL,
  [INFORMACION1] [varchar](200) NULL,
  [INFORMACION2] [varchar](200) NULL
)
ON [PRIMARY]
GO