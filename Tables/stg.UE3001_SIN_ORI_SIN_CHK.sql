﻿CREATE TABLE [stg].[UE3001_SIN_ORI_SIN_CHK] (
  [NOMBRE1] [nvarchar](100) NULL,
  [NOMBRE2] [nvarchar](100) NULL,
  [APELLIDO1] [nvarchar](100) NULL,
  [APELLIDO2] [nvarchar](100) NULL,
  [EMAIL] [nvarchar](100) NULL,
  [TELEFONO1] [nvarchar](50) NULL,
  [DNI] [nvarchar](50) NULL,
  [ORIGEN] [nvarchar](50) NULL
)
ON [PRIMARY]
GO