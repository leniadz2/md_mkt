﻿CREATE TABLE [stg].[UE3001_CON_ORI_CON_CHK] (
  [ORIGEN] [varchar](50) NULL,
  [FECREGISTRO] [varchar](50) NULL,
  [NOMBRES] [varchar](200) NULL,
  [APELLIDOS] [varchar](200) NULL,
  [EDAD] [varchar](50) NULL,
  [DNI] [varchar](50) NULL,
  [EMAIL] [varchar](100) NULL,
  [TELEFONO] [varchar](50) NULL,
  [DISTRITO] [varchar](150) NULL,
  [FLG_TERM_COND] [varchar](50) NULL
)
ON [PRIMARY]
GO