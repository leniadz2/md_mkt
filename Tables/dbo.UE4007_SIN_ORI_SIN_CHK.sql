﻿CREATE TABLE [dbo].[UE4007_SIN_ORI_SIN_CHK] (
  [NOMBRE 1] [nvarchar](50) NULL,
  [NOMBRE2] [nvarchar](50) NULL,
  [APELLIDO1] [nvarchar](50) NULL,
  [APELLIDO2] [nvarchar](50) NULL,
  [EMAIL] [nvarchar](100) NULL,
  [TELEFONO_1] [nvarchar](50) NULL,
  [DNI] [nvarchar](50) NULL,
  [ORIGEN] [nvarchar](50) NULL
)
ON [PRIMARY]
GO