﻿CREATE TABLE [ods].[CLIENTES_STP01] (
  [UNIDADECONOMICA] [varchar](10) NULL,
  [FLGORIGEN] [varchar](10) NULL,
  [FLGCHECK] [varchar](10) NULL,
  [CAMPANA] [varchar](50) NULL,
  [ID] [varchar](50) NULL,
  [FECREGISTRO] [varchar](50) NULL,
  [DNI] [varchar](50) NULL,
  [NOMBRE1] [nvarchar](100) NULL,
  [NOMBRE2] [nvarchar](100) NULL,
  [NOMBRES] [varchar](200) NULL,
  [APELLIDO1] [nvarchar](100) NULL,
  [APELLIDO2] [nvarchar](100) NULL,
  [APELLIDOS] [varchar](200) NULL,
  [NOMBRES_APELLIDOS] [varchar](400) NULL,
  [TELEFONO] [varchar](50) NULL,
  [EMAIL] [nvarchar](100) NULL,
  [DIRECCION] [varchar](400) NULL,
  [DISTRITO] [varchar](150) NULL,
  [FECNACIMIENTO] [varchar](50) NULL,
  [EDAD] [varchar](50) NULL,
  [SEXO] [varchar](50) NULL,
  [INFORMACION1] [varchar](200) NULL,
  [INFORMACION2] [varchar](200) NULL,
  [FLG_TERM_COND] [varchar](50) NULL
)
ON [PRIMARY]
GO