﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [stg].[sp_mkt]
AS
/***************************************************************************************************
Procedure:          [stg].sp_mkt
Create Date:        20210826
Author:             dÁlvarez
Description:        Carga la tablas MKT
Call by:            none
Affected table(s):  tbd
Used By:            BI
Parameter(s):       none
Log:                tbd
Prerequisites:      tbd
****************************************************************************************************
SUMMARY OF CHANGES
Date(YYYYMMDD)      Author              Comments
------------------- ------------------- ------------------------------------------------------------
20210826            dÁlvarez            creación
20211112            dÁlvarez            nuevos archivos MKT

***************************************************************************************************/

--SELECT * 
--  INTO stg.CLIENTES
--  FROM [DL_CLNT].stg.MKT_CLI;

------------------------------------------------------------------
INSERT INTO ods.CLIENTES_STP01 ( UNIDADECONOMICA
                          ,FLGORIGEN
                          ,FLGCHECK
                          ,CAMPANA
                          ,FECREGISTRO
                          ,NOMBRES
                          ,APELLIDOS
                          ,EDAD
                          ,DNI
                          ,EMAIL
                          ,TELEFONO
                          ,DISTRITO
                          ,FLG_TERM_COND)
SELECT '3001CCPN'
      ,'1'
      ,'1'
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(ORIGEN       ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(FECREGISTRO  ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(NOMBRES      ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(APELLIDOS    ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(EDAD         ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(DNI          ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(EMAIL        ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(TELEFONO     ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(DISTRITO     ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(FLG_TERM_COND))
  FROM stg.UE3001_CON_ORI_CON_CHK;
  
INSERT INTO ods.CLIENTES_STP01 ( UNIDADECONOMICA
                          ,FLGORIGEN
                          ,FLGCHECK
                          ,CAMPANA
                          ,ID
                          ,NOMBRES
                          ,APELLIDOS
                          ,NOMBRES_APELLIDOS
                          ,DNI
                          ,EDAD
                          ,SEXO
                          ,TELEFONO
                          ,DIRECCION
                          ,DISTRITO
                          ,FECNACIMIENTO
                          ,EMAIL
                          ,FECREGISTRO
                          ,INFORMACION1
                          ,INFORMACION2)
SELECT '3001CCPN'
      ,'1'
      ,'0'
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(CAMPANA          ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(ID               ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(NOMBRES          ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(APELLIDOS        ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(NOMBRES_APELLIDOS))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(DNI              ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(EDAD             ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(SEXO             ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(TELEFONO         ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(DIRECCION        ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(DISTRITO         ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(FECNACIMIENTO    ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(EMAIL            ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(FECREGISTRO      ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(INFORMACION1     ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(INFORMACION2     ))
  FROM stg.UE3001_CON_ORI_SIN_CHK;


INSERT INTO ods.CLIENTES_STP01 ( UNIDADECONOMICA
                          ,FLGORIGEN
                          ,FLGCHECK
                          ,NOMBRE1
                          ,NOMBRE2
                          ,APELLIDO1
                          ,APELLIDO2
                          ,EMAIL
                          ,TELEFONO
                          ,DNI
                          ,CAMPANA)
SELECT '3001CCPN'
      ,'0'
      ,'0'
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(NOMBRE1  ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(NOMBRE2  ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(APELLIDO1))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(APELLIDO2))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(EMAIL    ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(TELEFONO1))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(DNI      ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(ORIGEN   ))
  FROM stg.UE3001_SIN_ORI_SIN_CHK;
  
------------------------------------------------------------------
INSERT INTO ods.CLIENTES_STP01 ( UNIDADECONOMICA
                          ,FLGORIGEN
                          ,FLGCHECK
                          ,CAMPANA
                          ,FECREGISTRO
                          ,NOMBRES
                          ,APELLIDOS
                          ,EDAD
                          ,DNI
                          ,EMAIL
                          ,TELEFONO
                          ,DISTRITO
                          ,FLG_TERM_COND)
SELECT '4007MDS'
      ,'1'
      ,'1'
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(ORIGEN       ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(FECREGISTRO  ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(NOMBRES      ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(APELLIDOS    ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(EDAD         ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(DNI          ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(EMAIL        ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(TELEFONO     ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(DISTRITO     ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(FLG_TERM_COND))
  FROM stg.UE4007_CON_ORI_CON_CHK;
  
INSERT INTO ods.CLIENTES_STP01 ( UNIDADECONOMICA
                          ,FLGORIGEN
                          ,FLGCHECK
                          ,CAMPANA
                          ,ID
                          ,NOMBRES
                          ,APELLIDOS
                          ,NOMBRES_APELLIDOS
                          ,DNI
                          ,EDAD
                          ,SEXO
                          ,TELEFONO
                          ,DIRECCION
                          ,DISTRITO
                          ,FECNACIMIENTO
                          ,EMAIL
                          ,FECREGISTRO
                          ,INFORMACION1
                          ,INFORMACION2)
SELECT '4007MDS'
      ,'1'
      ,'0'
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(CAMPANA          ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(ID               ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(NOMBRES          ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(APELLIDOS        ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(NOMBRES_APELLIDOS))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(DNI              ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(EDAD             ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(SEXO             ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(TELEFONO         ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(DIRECCION        ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(DISTRITO         ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(FECNACIMIENTO    ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(EMAIL            ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(FECREGISTRO      ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(INFORMACION1     ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(INFORMACION2     ))
  FROM stg.UE4007_CON_ORI_SIN_CHK;


INSERT INTO ods.CLIENTES_STP01 ( UNIDADECONOMICA
                          ,FLGORIGEN
                          ,FLGCHECK
                          ,NOMBRE1
                          ,NOMBRE2
                          ,APELLIDO1
                          ,APELLIDO2
                          ,EMAIL
                          ,TELEFONO
                          ,DNI
                          ,CAMPANA)
SELECT '4007MDS'
      ,'0'
      ,'0'
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(NOMBRE1  ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(NOMBRE2  ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(APELLIDO1))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(APELLIDO2))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(EMAIL    ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(TELEFONO1))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(DNI      ))
      ,ods.fn_UTF8_to_NVARCHAR(TRIM(ORIGEN   ))
  FROM stg.UE4007_SIN_ORI_SIN_CHK;


--------------


SELECT UNIDADECONOMICA
      ,IIF(c.FLGORIGEN         = '',NULL,c.FLGORIGEN)                AS FLGORIGEN        
      ,IIF(c.FLGCHECK          = '',NULL,c.FLGCHECK)                 AS FLGCHECK         
      ,IIF(c.CAMPANA           = '',NULL,UPPER(c.CAMPANA))           AS CAMPANA          
      ,IIF(c.ID                = '',NULL,UPPER(c.ID))                AS ID               
      ,IIF(c.FECREGISTRO       = '',NULL,c.FECREGISTRO)              AS FECREGISTRO      
      ,ods.fn_ajustaDNI(ods.fn_StripCharacters(c.DNI, '^0-9')) AS DNI
      ,IIF(c.NOMBRE1           = '',NULL,UPPER(c.NOMBRE1))           AS NOMBRE1          
      ,IIF(c.NOMBRE2           = '',NULL,UPPER(c.NOMBRE2))           AS NOMBRE2          
      ,IIF(c.NOMBRES           = '',NULL,UPPER(c.NOMBRES))           AS NOMBRES          
      ,IIF(c.APELLIDO1         = '',NULL,UPPER(c.APELLIDO1))         AS APELLIDO1        
      ,IIF(c.APELLIDO2         = '',NULL,UPPER(c.APELLIDO2))         AS APELLIDO2        
      ,IIF(c.APELLIDOS         = '',NULL,UPPER(c.APELLIDOS))         AS APELLIDOS        
      ,IIF(c.NOMBRES_APELLIDOS = '',NULL,UPPER(c.NOMBRES_APELLIDOS)) AS NOMBRES_APELLIDOS
      ,IIF(c.TELEFONO          = '',NULL,c.TELEFONO)                 AS TELEFONO         
      ,IIF(c.EMAIL             = '',NULL,c.EMAIL)                    AS EMAIL            
      ,IIF(c.DIRECCION         = '',NULL,UPPER(c.DIRECCION))         AS DIRECCION        
      ,IIF(c.DISTRITO          = '',NULL,UPPER(c.DISTRITO))          AS DISTRITO         
      ,IIF(c.FECNACIMIENTO     = '',NULL,c.FECNACIMIENTO)            AS FECNACIMIENTO    
      ,IIF(c.EDAD              = '',NULL,c.EDAD)                     AS EDAD             
      ,IIF(c.SEXO              = '',NULL,c.SEXO)                     AS SEXO             
      ,IIF(c.INFORMACION1      = '',NULL,c.INFORMACION1)             AS INFORMACION1     
      ,IIF(c.INFORMACION2      = '',NULL,c.INFORMACION2)             AS INFORMACION2     
      ,IIF(c.FLG_TERM_COND     = '',NULL,c.FLG_TERM_COND)            AS FLG_TERM_COND    
INTO ods.CLIENTES_STP02
FROM MD_MKT.ods.CLIENTES_STP01 c;



-----------
  UPDATE MD_MKT.ods.CLIENTES_STP02
   SET FECREGISTRO = CAST(FECREGISTRO - 2 as SmallDateTime)
 WHERE LEN(FECREGISTRO) = 5;

UPDATE MD_MKT.ods.CLIENTES_STP02
   SET FECNACIMIENTO = CAST(FECNACIMIENTO - 2 as SmallDateTime)
 WHERE LEN(FECNACIMIENTO) = 5;


SELECT c.UNIDADECONOMICA
      ,c.FLGORIGEN
      ,c.FLGCHECK
      ,c.CAMPANA
      ,c.ID
--      ,IIF( LEN(ods.fn_RemoveNumberChar(SUBSTRING(c.FECREGISTRO,3,1)))>0
--           ,CONCAT(SUBSTRING(c.FECREGISTRO,7,4),SUBSTRING(c.FECREGISTRO,4,2),SUBSTRING(c.FECREGISTRO,1,2))
--           ,IIF(LEN(c.FECREGISTRO)>0,CONCAT(SUBSTRING(c.FECREGISTRO,1,4),SUBSTRING(c.FECREGISTRO,6,2),SUBSTRING(c.FECREGISTRO,9,2)),NULL)) AS FECREGISTRO
        ,IIF( LEN(ods.fn_RemoveNumberChar(SUBSTRING(c.FECREGISTRO,2,1)))>0 OR SUBSTRING(c.FECREGISTRO,2,1)=' '
           ,CONCAT(SUBSTRING(c.FECREGISTRO,6,4),SUBSTRING(c.FECREGISTRO,3,2),'0',SUBSTRING(c.FECREGISTRO,1,1))
           ,IIF( LEN(ods.fn_RemoveNumberChar(SUBSTRING(c.FECREGISTRO,3,1)))>0 OR SUBSTRING(c.FECREGISTRO,3,1)=' '
                ,CONCAT(SUBSTRING(c.FECREGISTRO,7,4),SUBSTRING(c.FECREGISTRO,4,2),SUBSTRING(c.FECREGISTRO,1,2))
                ,IIF(LEN(c.FECREGISTRO)>0,CONCAT(SUBSTRING(c.FECREGISTRO,1,4),SUBSTRING(c.FECREGISTRO,6,2),SUBSTRING(c.FECREGISTRO,9,2)),NULL))) AS FECREGISTRO
      ,SUBSTRING(c.DNI,3,LEN(c.DNI)-2) AS DUI
      ,'01' AS ID_TIPODUI
      ,SUBSTRING(c.DNI,1,1) AS DUIval
--      ,c.NOMBRE1
--      ,c.NOMBRE2
--      ,c.NOMBRES
      ,ISNULL(c.NOMBRES,IIF(LEN(CONCAT(c.NOMBRE1,c.NOMBRE2))>0,CONCAT(c.NOMBRE1,' ',c.NOMBRE2),NULL)) AS NOMBRES
--      ,c.APELLIDO1
--      ,c.APELLIDO2
--      ,c.APELLIDOS
      ,ISNULL(c.APELLIDOS,IIF(LEN(CONCAT(c.APELLIDO1,c.APELLIDO2))>0,CONCAT(c.APELLIDO1,' ',c.APELLIDO2),NULL)) AS APELLIDOS
      ,c.NOMBRES_APELLIDOS
      ,c.TELEFONO
      ,IIF(LEN(c.TELEFONO)>0,ods.fn_validaPhone(RIGHT(c.TELEFONO,9)),NULL) AS TIPOTELEFONO
      ,c.EMAIL
      ,c.DIRECCION
      ,c.DISTRITO
--      ,IIF( LEN(ods.fn_RemoveNumberChar(SUBSTRING(c.FECNACIMIENTO,3,1)))>0
--           ,CONCAT(SUBSTRING(c.FECNACIMIENTO,7,4),SUBSTRING(c.FECNACIMIENTO,4,2),SUBSTRING(c.FECNACIMIENTO,1,2))
--           ,IIF(LEN(c.FECNACIMIENTO)>0,CONCAT(SUBSTRING(c.FECNACIMIENTO,1,4),SUBSTRING(c.FECNACIMIENTO,6,2),SUBSTRING(c.FECNACIMIENTO,9,2)),NULL)) AS FECNACIMIENTO
      ,IIF( LEN(ods.fn_RemoveNumberChar(SUBSTRING(c.FECNACIMIENTO,2,1)))>0 OR SUBSTRING(c.FECNACIMIENTO,2,1) = ' '
           ,CONCAT(SUBSTRING(c.FECNACIMIENTO,6,4),SUBSTRING(c.FECNACIMIENTO,3,2),'0',SUBSTRING(c.FECNACIMIENTO,1,1))
           ,IIF( LEN(ods.fn_RemoveNumberChar(SUBSTRING(c.FECNACIMIENTO,3,1)))>0 OR SUBSTRING(c.FECNACIMIENTO,3,1) = ' '
                ,CONCAT(SUBSTRING(c.FECNACIMIENTO,7,4),SUBSTRING(c.FECNACIMIENTO,4,2),SUBSTRING(c.FECNACIMIENTO,1,2))
                ,IIF(LEN(c.FECNACIMIENTO)>0,CONCAT(SUBSTRING(c.FECNACIMIENTO,1,4),SUBSTRING(c.FECNACIMIENTO,6,2),SUBSTRING(c.FECNACIMIENTO,9,2)),NULL))) AS FECNACIMIENTO

      ,IIF(LEN(c.EDAD)>0,ods.fn_StripCharacters(c.EDAD, '^0-9'),NULL) AS EDAD
--      ,c.SEXO
      ,CASE SUBSTRING(sexo,1,2)
         WHEN 'F' THEN 'FEMENINO'
         WHEN 'FE' THEN 'FEMENINO'
         WHEN 'MU' THEN 'FEMENINO'
         WHEN 'H' THEN 'MASCULINO'
         WHEN 'HO' THEN 'MASCULINO'
         WHEN 'MA' THEN 'MASCULINO'
         WHEN 'M' THEN 'MASCULINO'
       END AS SEXO
      ,c.INFORMACION1
      ,c.INFORMACION2
      ,c.FLG_TERM_COND
  INTO bds.CLIENTES
  FROM ods.CLIENTES_STP02 c;

------------
  UPDATE bds.CLIENTES
     SET FECREGISTRO = NULL
   WHERE LEN(ods.fn_RemoveNumberChar(FECREGISTRO)) > 0;

  UPDATE bds.CLIENTES
     SET FECNACIMIENTO = NULL
   WHERE LEN(ods.fn_RemoveNumberChar(FECNACIMIENTO)) > 0;


GO